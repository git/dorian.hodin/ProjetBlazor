﻿using ProjetBlazor.Models;

namespace ProjetBlazor.Components
{
    public class CraftingRecipe
    {
        public Item Give { get; set; }
        public List<List<string>> Have { get; set; }

    }
}
